// @dart=2.9

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_method/All_Constants/MyIconButton.dart';
import 'package:login_method/All_Constants/background_image.dart';
import 'package:login_method/All_Constants/color_constants.dart';
import 'package:login_method/All_Constants/custom_button.dart';
import 'package:login_method/All_Constants/custom_toast.dart';
import 'package:login_method/All_Constants/input_field.dart';
import 'package:login_method/login_methods/registration.dart';
import 'package:login_method/screen/home_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool obsecure = true;
  TextEditingController _emailtexteditingController;
  TextEditingController _passwordtextEditingController;
  final _auth = FirebaseAuth.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _emailtexteditingController = TextEditingController();
    _passwordtextEditingController = TextEditingController();
  }


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(child: backgroundImage()),
          Expanded(child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                InputField(
                  textEditingController: _emailtexteditingController,
                  hintText: 'Enter your e-mail',
                  obsecure: false,
                  leadIcon: Icons.email,
                  keyBoard: TextInputType.emailAddress,
                ),
                SizedBox(height: size.height * 0.03,),
                InputField(
                    textEditingController: _passwordtextEditingController,
                    hintText: 'Enter password',
                    obsecure: obsecure,
                    leadIcon: Icons.lock,
                    keyBoard: TextInputType.text,
                    suffixIcon: obsecure == true ? IconButton(
                      icon: Icon(Icons.remove_red_eye),
                      onPressed: () {
                        setState(() {
                          obsecure = false;
                        });
                      },
                    ) : IconButton(
                      icon: Icon(Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          obsecure = true;
                        });
                      },
                    )
                ),
                SizedBox(height: size.height * 0.03,),

                CustomButton(
                  text: "Sign In",
                  mainColor: Colors.deepPurple,
                  borderColor: Colors.white,
                  onpress: () async {
                    try {
                      final logedUser = await _auth.signInWithEmailAndPassword(
                          email: _emailtexteditingController.text,
                          password: _passwordtextEditingController.text);
                      if (logedUser != null) {
                        Navigator.pushAndRemoveUntil(context,
                            MaterialPageRoute(
                              builder: (context) {
                                return HomePage();
                              },
                            ), (route) => false);
                        CustomToast.toast(' Successfull');
                      }
                    } catch (error) {
                      print(error.toString());
                      CustomToast.toast('Invalid');
                    }
                  },
                ),
                SizedBox(height: size.height * 0.03,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Don\'t have an account?\n",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .subtitle1,
                            ),
                            TextSpan(
                              text: "Tap to Register ",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .button
                                  .copyWith(color: kSecondaryColor),
                            )
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context, MaterialPageRoute(builder: (context) {
                            return RegistrationPage();
                          },),);
                          CustomToast.toast('Clicked');
                        },
                        child: MyIconButton(svg: "assets/icons/Goarrow.svg"),
                      ),


                    ],
                  ),

                ),
              ],
            ),
          ),),
        ],
      ),
    );
  }
}
