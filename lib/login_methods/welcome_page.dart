import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_method/All_Constants/MyIconButton.dart';
import 'package:login_method/All_Constants/background_image.dart';
import 'package:login_method/All_Constants/color_constants.dart';
import 'package:login_method/login_methods/login_with_email&password.dart';

class WelcomePage extends StatefulWidget {

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Firebase.initializeApp();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
              flex: 5,
              child: backgroundImageWithText()),
          Expanded(child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Continue With', style: Theme
                    .of(context)
                    .textTheme
                    .button,),
                Padding(padding: EdgeInsets.only(bottom: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      MyIconButton(svg: "assets/icons/Iconfacebook.svg"),
                      MyIconButton(svg: "assets/icons/Icontwitter.svg",),
                      MyIconButton(svg: "assets/icons/icons-google.svg",),
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 25.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25.0),
                          color: kBlackColor,
                        ),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context, MaterialPageRoute(builder: (context) {
                              return LoginPage();
                            },),);
                          },
                          child: Text('E-mail', style: Theme
                              .of(context)
                              .textTheme
                              .button!
                              .copyWith(color: kWhiteColor),),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ))
        ],
      ),
    );
  }
}
