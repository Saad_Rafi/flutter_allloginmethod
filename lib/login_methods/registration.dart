//@dart=2.9
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_method/All_Constants/MyIconButton.dart';
import 'package:login_method/All_Constants/background_image.dart';
import 'package:login_method/All_Constants/color_constants.dart';
import 'package:login_method/All_Constants/custom_button.dart';
import 'package:login_method/All_Constants/custom_toast.dart';
import 'package:login_method/All_Constants/input_field.dart';
import 'package:login_method/screen/home_page.dart';

import 'login_with_email&password.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  bool obsecure = true;
  TextEditingController _nametextEditingController ;
  TextEditingController _emailtexteditingController;
  TextEditingController _passwordtextEditingController;

  final _auth = FirebaseAuth.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nametextEditingController = TextEditingController();
    _emailtexteditingController = TextEditingController();
    _passwordtextEditingController = TextEditingController();
  }



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(child: backgroundImage()),
          SingleChildScrollView(
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  InputField(
                    textEditingController: _nametextEditingController,
                    hintText: 'UserName',
                    obsecure: false,
                    leadIcon: Icons.person,
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  InputField(
                    textEditingController: _emailtexteditingController,
                    hintText: 'Enter your e-mail',
                    obsecure: false,
                    leadIcon: Icons.email,
                    keyBoard: TextInputType.emailAddress,
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  InputField(
                      textEditingController: _passwordtextEditingController,
                      hintText: 'Enter password',
                      obsecure: obsecure,
                      leadIcon: Icons.lock,
                      keyBoard: TextInputType.text,
                      suffixIcon: obsecure == true
                          ? IconButton(
                              icon: Icon(Icons.remove_red_eye),
                              onPressed: () {
                                setState(() {
                                  obsecure = false;
                                });
                              },
                            )
                          : IconButton(
                              icon: Icon(Icons.visibility_off),
                              onPressed: () {
                                setState(() {
                                  obsecure = true;
                                });
                              },
                            )),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  CustomButton(
                    text: "Sign Up",
                    mainColor: Colors.deepPurple,
                    borderColor: Colors.white,
                    onpress: () async {
                      try {
                        final newUser =
                            await _auth.createUserWithEmailAndPassword(
                                email: _emailtexteditingController.text,
                                password: _passwordtextEditingController.text);
                        if (newUser != null) {
                          Navigator.pushAndRemoveUntil(context,
                              MaterialPageRoute(
                            builder: (context) {
                              return HomePage();
                            },
                          ), (route) => false);
                          CustomToast.toast('Registration Successfull');
                        }
                      } catch (error) {
                        print(error.toString());
                      }
                    },
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Already have an account?\n",
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                              TextSpan(
                                text: "Tap to login ",
                                style: Theme.of(context)
                                    .textTheme
                                    .button
                                    .copyWith(color: kSecondaryColor),
                              )
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return LoginPage();
                                },
                              ),
                            );
                            CustomToast.toast('Clicked');
                          },
                          child: MyIconButton(svg: "assets/icons/Goarrow.svg"),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
