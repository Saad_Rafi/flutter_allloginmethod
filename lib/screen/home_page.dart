//@dart=2.9
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:login_method/All_Constants/custom_button.dart';
import 'package:login_method/All_Constants/custom_toast.dart';
import 'package:login_method/All_Constants/input_field.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool obsecure = true;
  String _messageText;
  String _emailText;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  TextEditingController _nametextEditingController = new TextEditingController();
  TextEditingController _emailtexteditingController = new TextEditingController();


  sendUserData() async{
    Map<String,dynamic> data={
      'name':_nametextEditingController.text,
      'email':_emailtexteditingController.text,
    };
    FirebaseFirestore.instance.collection("test").add(data);
  }


  @override
  Widget build(BuildContext context) {

    // Create a CollectionReference called users that references the firestore collection
  /*  CollectionReference users = FirebaseFirestore.instance.collection('test');

    Future<void> addUser() {
      // Call the user's CollectionReference to add a new user
      return users
          .add({
        'full_name': _messageText, // John Doe
        'company': _emailText, // Stokes and Sons
         // 42
      })
          .then((value) => print("data Added"))
          .catchError((error) => print("Failed to add user: $error"));
    }*/


    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SingleChildScrollView(
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  InputField(
                    // userTyped: (value){
                    //   _messageText=value;
                    // },
                    textEditingController: _nametextEditingController,
                    hintText: 'UserName',
                    obsecure: false,
                    leadIcon: Icons.person,
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  InputField(
                    // userTyped: (value){
                    //   _emailText=value;
                    // },
                    textEditingController: _emailtexteditingController,
                    hintText: 'Enter your e-mail',
                    obsecure: false,
                    leadIcon: Icons.email,
                    keyBoard: TextInputType.emailAddress,
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  CustomButton(
                    text: "Sent",
                    mainColor: Colors.deepPurple,
                    borderColor: Colors.white,
                    onpress: ()  {
                      // Map<String,dynamic> data={
                      //   'name':_nametextEditingController.text,
                      //   'email':_emailtexteditingController.text,
                      // };
                      /*_firestore.collection('test').add({
                        'text':_messageText,
                        'email':_emailText
                      });*/

                      // addUser();
                          sendUserData();
                          CustomToast.toast('Data Send');

                    },
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
