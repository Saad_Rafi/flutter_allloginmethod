import 'package:flutter/material.dart';
import 'package:login_method/All_Constants/color_constants.dart';

Stack backgroundImage() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [kPrimaryColor, kWhiteColor],
          ),
        ),
      ),
      Positioned(
        right: 0,
        left: 0,
        bottom: 0,
        child: Container(
          height: 400,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover),
          ),
        ),
      ),
    ],
  );
}

Stack backgroundImageWithText() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [kPrimaryColor, kWhiteColor])),
      ),
      Positioned(
        top: 130,
        left: 16,
        right: 0,
        child: Text(
          'Your Adventure Begins Now',
          style: TextStyle(
              fontSize: 50, fontWeight: FontWeight.bold, height: 1.32),
        ),
      ),
      Positioned(
        top: 0,
        right: 0,
        bottom: 0,
        child: Container(
          height: 400,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bg.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    ],
  );
}
