// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputField extends StatelessWidget{
  final String hintText;
  final IconData leadIcon;
  final IconButton suffixIcon;
  final Function userTyped;
  final TextInputType keyBoard;
  final bool obsecure;
  final TextEditingController textEditingController;

  InputField({
    this.hintText,
    this.textEditingController,
    this.keyBoard,
    this.leadIcon,
    this.obsecure,
    this.suffixIcon,
    this.userTyped,
});

  @override
  Widget build(BuildContext context) {
     return Container(
       margin: EdgeInsets.symmetric(horizontal: 15.0),
       decoration: BoxDecoration(
         color: Colors.grey.shade200,
         borderRadius: BorderRadius.circular(16),
       ),
       padding: EdgeInsets.only(left: 10.0),
       width: MediaQuery.of(context).size.width * 0.8,
       child: TextField(
         controller: textEditingController,
         onChanged: userTyped,
         keyboardType: keyBoard,
         onSubmitted: (value){},
         autofocus: false,
         obscureText: obsecure,
         decoration: InputDecoration(
           icon: Icon(leadIcon,color: Colors.deepPurple,),
           border: InputBorder.none,
           hintText: hintText,
           suffixIcon: suffixIcon,
         ),
       ),
     );
  }
}