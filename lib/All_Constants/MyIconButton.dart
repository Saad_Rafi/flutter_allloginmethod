//@dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:login_method/All_Constants/color_constants.dart';


class MyIconButton extends StatelessWidget {
  final String svg;
  final double size;

  MyIconButton({this.svg, this.size = 60});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: kBlackColor,
      ),
      child: SvgPicture.asset(svg),
    );
  }
}
