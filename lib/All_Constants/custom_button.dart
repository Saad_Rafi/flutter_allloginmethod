//@dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_method/main.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Color mainColor;
  final Color borderColor;
  final Function() onpress;

  CustomButton({this.borderColor, this.mainColor, this.onpress, this.text});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onpress,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          border: Border.all(color: borderColor),
          color: mainColor,
          borderRadius: BorderRadius.circular(16),
        ),
        width: MediaQuery.of(context).size.width * 0.8,
        padding: EdgeInsets.all(15.0),
        child: Center(
          child: Text(
            text.toUpperCase(),
            style: TextStyle(color: borderColor),
          ),
        ),
      ),
    );
  }
}
